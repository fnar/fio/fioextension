(function() {
	if (window.FIO_HAS_RUN === true) {
		return true;
	}
	window.FIO_HAS_RUN = true;
	
	console.log("FIO: ContentScript Initialized");
	var fnar_username = "";
	var fnar_password = "";

	CheckCredentials();

	var currentDatabaseVersion = 2;

	function CheckCredentials(){
		chrome.storage.local.get(['databaseVersion'], function(result) {
			if(result.databaseVersion != null && result.databaseVersion == currentDatabaseVersion)
			{
				chrome.storage.local.get([
					'username',
					'password'],
					function(result) {
						if(result.username != null && result.password != null) 
						{
							fnar_username = result.username;
							fnar_password = result.password;
							
							var credentialsAndExtensionId = "var fnar_username = '" + fnar_username + "';" + "var fnar_password = '" + fnar_password.replace(/'/g, "\\'") + "';var fnar_extension_id = '" + chrome.runtime.id + "';\n";
							var credentialsAndExtensionIdScript = document.createElement("script");
							credentialsAndExtensionIdScript.textContent = credentialsAndExtensionId;
							
							var uploaderScript = document.createElement("script");
							uploaderScript.src = chrome.runtime.getURL("uploader.js");
							
							var collectorScript = document.createElement("script");
							collectorScript.src = chrome.runtime.getURL("collector.js");
							
							FIO_Loader(credentialsAndExtensionIdScript);
							console.log("FIO: Loaded credsAndExtensionId.js");
							
							FIO_Loader(uploaderScript);
							console.log("FIO: Loaded uploader.js");

							FIO_Loader(collectorScript);
							console.log("FIO: Loaded collector.js");
						}
						else {
							LoadRegisterPath();
						}
				   });
			}
			else {
				chrome.storage.local.clear(function(result){});
				LoadRegisterPath();
				
			}
		});
	}

	function LoadRegisterPath()
	{
		var ExtensionIdScriptTxt = "var fnar_extension_id = '" + chrome.runtime.id + "';\n";
		var extensionIdScript = document.createElement("script");
		extensionIdScript.textContent = ExtensionIdScriptTxt;
		FIO_Loader(extensionIdScript);

		var registrationScript = document.createElement("script");
		registrationScript.src = chrome.runtime.getURL("register.js");
		FIO_Loader(registrationScript);
		
		console.log("FIO: Loaded Register Path");
	}

	function FIO_Loader(script){
		(document.head||document.documentElement).appendChild(script);
	}

	var port = chrome.runtime.connect();
	port.onDisconnect.addListener(function()
	{
		console.warn("FIO: PortOnDisconnect - " + chrome.runtime.lastError);
	})

	window.addEventListener("message", function(msg) {	
		if (msg.data)
			port.postMessage(msg.data);
	}, false);
	
})();
