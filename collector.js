(function() {
    try{
        Connect();
    }
    catch(error){
        console.error(error);
    }
})();

// Function has to get called very early before PrUn otherwise it will not work
function Connect()
{
    'use strict';

    var OrigWebSocket = window.WebSocket;
    var callWebSocket = OrigWebSocket.apply.bind(OrigWebSocket);
    var wsAddListener = OrigWebSocket.prototype.addEventListener;
    wsAddListener = wsAddListener.call.bind(wsAddListener);
    window.WebSocket = function WebSocket(url, protocols)
    {	
        var ws;
        if (!(this instanceof WebSocket))
        {
            // Called without 'new' (browsers will throw an error).
            ws = callWebSocket(this, arguments);
        }
        else if (arguments.length === 1)
        {
            ws = new OrigWebSocket(url);
        }
        else if (arguments.length >= 2)
        {
            ws = new OrigWebSocket(url, protocols);
        }
        else
        {
            ws = new OrigWebSocket();
        }

        wsAddListener(ws, 'message', function(event)
        {			
            try
			{
				ProcessMessage(event);
            }      
            catch(error)
			{
				//console.error("FIO: Failed to process message");
            }
        });
		
		wsAddListener(ws, 'close', function(event)
		{
			console.log("FIO: WebSocket closed: " + event.reason);
		});
		console.log("FIO: Message listener added");
        return ws;
    }.bind();
	
    window.WebSocket.prototype = OrigWebSocket.prototype;
    window.WebSocket.prototype.constructor = window.WebSocket;

    var wsSend = OrigWebSocket.prototype.send;
    wsSend = wsSend.apply.bind(wsSend);
    OrigWebSocket.prototype.send = function(data)
    {
        return wsSend(this, arguments);
    };
}