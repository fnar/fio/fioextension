var fnar_url = "https://rest.fnar.net";
var fio_url = "https://fio.fnar.net";

function SendRegister(username, registrationGuid){
    try {
        window.postMessage({command: "Register", UserName: username, RegistrationGuid: registrationGuid}, "*");
    } catch (error) {
        console.warn(error);
    }
}

function fnar_send_auth_data(url, data)
{	
    var fnarhttp = new XMLHttpRequest();
    fnarhttp.onreadystatechange = function()
    {
        if (this.readyState === XMLHttpRequest.DONE)
        {
            var status = this.status;
            if (status === 0 || status == 200)
            {
				// The request has been completed successfully
                console.log("FIO: Received Registration Payload.  Redirecting...");
				var json = JSON.parse(this.response);
				var username = json.UserName;
				var registrationGuid = json.RegistrationGuid;
				SendRegister(username, registrationGuid);
            }
			else if (status === 204)
			{
				console.log("FIO: User already exists. Awaiting user login.");
			}
            else
            {
                console.error("FIO: Registration send failed " + status);
            }
        }
    };
    fnarhttp.withCredentials = false;
    fnarhttp.open("POST", url, true);
    fnarhttp.setRequestHeader("Content-type", "application/json");
	if (data != null)
	{
		fnarhttp.send(JSON.stringify(data));
	}
}

var checkExist = null;
window.addEventListener("DOMContentLoaded", function() {
	console.log("FIO: DOMContentLoaded hit");
	checkExist = setInterval(function() 
	{
		console.log("FIO: Checking ReactContainer...");
		var model = document.getElementById('container')._reactRootContainer._internalRoot.current.child.child.child.pendingProps.store.getState().toJS();
		if ( model !== undefined && model.user !== undefined && model.user.user !== undefined && model.user.user.data !== undefined && model.user.user.data.username !== undefined )
		{
			console.log("FIO: Found user. Sending autorequestregister.");
			clearInterval(checkExist);
			var autoregisterpayload = { messageType:"", payload:{username:model.user.user.data.username, admin:false} };
			fnar_send_auth_data(fnar_url + "/auth/autorequestregister", autoregisterpayload);
		}
	}, 1000);
});
