// Handle HTML5 PushState--this is like a half-refresh
function HandleOnHistoryStateUpdated(details) {
  //console.log("FIO: Detected PushState");
  //console.log(`\tURL: ${details.url}`);
  //console.log(`\tTransitionType: ${details.transitionType}`);
  //console.log(`\tTransitionQualifiers: ${details.transitionQualifiers}`);
  if (details.url.toLowerCase().includes("https://apex.prosperousuniverse.com"))
  {
	chrome.tabs.executeScript(null, {file:"contentscript.js"}, 
  _=>{
	console.log("FIO: Executing contentscript.js");
    let e = chrome.runtime.lastError;
    if(e !== undefined && e !== null){
      console.log("FIO: ExecuteScriptFailure: " + e);
    }
  });  
  }
}

console.log("FIO: Checking for existing listener");
if (chrome.webNavigation.onHistoryStateUpdated.hasListener(HandleOnHistoryStateUpdated)) {
	console.log("FIO: Unregistering existing HistoryState listener");
	chrome.webNavigation.onHistoryStateUpdated.removeListener(HandleOnHistoryStateUpdated);
}

console.log("FIO: Adding listener");
chrome.webNavigation.onHistoryStateUpdated.addListener(HandleOnHistoryStateUpdated);

var bHasPromptedRegister = false;

var backgroundPort;

chrome.runtime.onConnect.addListener(function(port) {
	backgroundPort = port;
	console.log("FIO: BackgroundPort Connected");
	
	backgroundPort.onMessage.addListener((msg) => {
		if (msg.command == "DisplayBadge") {
			chrome.browserAction.setIcon({path: "assets/fio-48.png"});
		}
		else if(msg.command == "HideBadge") {
			chrome.browserAction.setIcon({path: "assets/fio-48-dim.png"});
		}
		else if(msg.command == "Register") {
			if (!bHasPromptedRegister) {
				bHasPromptedRegister = true;
				chrome.tabs.create({ url: "https://fio.fnar.net/register?UserName=" + msg.UserName + "&RegistrationGuid=" + msg.RegistrationGuid, active:false }, function(tab) {
					chrome.tabs.update(tab.id, {active:true});
				});
			}
		}
	});
});

